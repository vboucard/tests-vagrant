# tests-vagrant

Le but du jeu est de tester l'installation offline d'une machine docker via une machine contenant les dépots

## Utilisation

Les machines seront des box vagrant, une avec accès internet et le logiciel apt-mirror et une autre offline sera une debian simple.

![alt text](plan.png "Title Text")
